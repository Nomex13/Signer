# Signer

Library to sign executables.

:warning: Not intended for production. :warning:

## Dependencies

Signer is dependent on the https://gitlab.com/iodynis/Encryptor project.

## Usage

Sign:

```csharp
byte[] privateKey = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
Signature.Sign(path, privateKey);
```

Check the signature is correct:

```csharp
byte[] publicKey = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
bool isCorrect = Signature.Check(Application.ExecutablePath, publicKey);
```

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.